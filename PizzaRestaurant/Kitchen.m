//
//  Kitchen.m
//  PizzaRestaurant
//
//  Created by Steven Masuch on 2014-07-19.
//  Copyright (c) 2014 Lighthouse Labs. All rights reserved.
//

#import "Kitchen.h"

PizzaSize parseSize(NSString *sizeString) {
    if ([sizeString isEqualToString:@"small"]) {
        return PizzaSizeSmall;
    }
    if ([sizeString isEqualToString:@"medium"]) {
        return PizzaSizeMedium;
    }
    return PizzaSizeLarge;
}

@implementation Kitchen

- (Pizza *)makePizzaWithSize:(PizzaSize)size toppings:(NSArray *)toppings
{
    return [[Pizza alloc] initWithSize:size toppings:toppings];
}

- (Pizza *)handleOrder:(NSArray<NSString *> *)order
{
    if ([order[0] isEqualToString:@"pepperoni"]) {
        return [Pizza largePepperoni];
    }

    PizzaSize inputSize = parseSize(order[0]);
    NSArray<NSString*>* inputToppings = [order subarrayWithRange:NSMakeRange(1, order.count - 1)];

    if ([inputToppings[0] isEqualToString:@"meatlovers"]) {
        return [Pizza meatLoversWithSize:inputSize];
    }

    return [self makePizzaWithSize:inputSize toppings:inputToppings];
}

@end
