//
//  Pizza.m
//  PizzaRestaurant
//
//  Created by James Cash on 09-03-17.
//  Copyright © 2017 Lighthouse Labs. All rights reserved.
//

#import "Pizza.h"

@implementation Pizza

+ (Pizza*)largePepperoni
{
    return [[Pizza alloc] initWithSize:PizzaSizeLarge toppings:@[@"pepperoni"]];
}

+ (Pizza*)meatLoversWithSize:(PizzaSize)size
{
    return [[Pizza alloc] initWithSize:size toppings:@[@"bacon", @"ham", @"pepperoni"]];
}

- (instancetype)initWithSize:(PizzaSize)size toppings:(NSArray<NSString*>*)toppings
{
    self = [super init];
    if (self) {
        _size = size;
        _toppings = toppings;
    }
    return self;
}

- (NSString*)sizeAsString
{
    switch (self.size) {
        case PizzaSizeSmall:
            return @"small";
        case PizzaSizeMedium:
            return @"medium";
        case PizzaSizeLarge:
            return @"large";
    }
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"A %@ pizza with %@", [self sizeAsString],
            [self.toppings componentsJoinedByString:@", "]];
}

@end
