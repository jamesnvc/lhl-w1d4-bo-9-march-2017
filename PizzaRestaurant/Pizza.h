//
//  Pizza.h
//  PizzaRestaurant
//
//  Created by James Cash on 09-03-17.
//  Copyright © 2017 Lighthouse Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PizzaSize) {
    PizzaSizeSmall,
    PizzaSizeMedium,
    PizzaSizeLarge
};

// NSInteger PizzaSizeSmall = 0;
// NSInteger PizzaSizeMedium = 1;

@interface Pizza : NSObject

@property (readonly,nonatomic,assign) PizzaSize size;
@property (readonly,nonatomic,strong) NSArray<NSString*>* toppings;

+ (Pizza*)largePepperoni;
+ (Pizza*)meatLoversWithSize:(PizzaSize)size;

- (instancetype)initWithSize:(PizzaSize)size toppings:(NSArray<NSString*>*)toppings;

@end
